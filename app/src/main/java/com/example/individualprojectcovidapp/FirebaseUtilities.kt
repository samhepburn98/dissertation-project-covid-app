package com.example.individualprojectcovidapp

import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class FirebaseUtilities {
    private var database = Firebase.database.reference.child("interactions")

    /**
     * Writes a new interaction object to the Firebase Realtime Database
     *
     * @param broadcasterId UUID of the device that broadcast the beacon
     * @param listenerId    UUID of the device that detected the beacon
     * @param dateTime      String representation of the date and time the beacon was detected
     * @param distance      Float representation of the calculated distance between the devices
     */
    fun writeNewInteraction(
        broadcasterId: String,
        listenerId: String,
        dateTime: String,
        distance: Float
    ) {
        val interaction = Interaction(broadcasterId, listenerId, dateTime, distance)
        database.push().setValue(interaction)
    }

    /**
     * Returns a string representation of the current date and time
     *
     * @return  String representation of current date and time
     */
    fun getCurrentDate(): String {
        val dateFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH)
        return dateFormat.format(Calendar.getInstance().time)
    }
}