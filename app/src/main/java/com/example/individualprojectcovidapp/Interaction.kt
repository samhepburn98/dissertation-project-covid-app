package com.example.individualprojectcovidapp

import com.google.firebase.database.IgnoreExtraProperties
import java.util.*

@IgnoreExtraProperties
data class Interaction(
    var broadcasterId: String? = null,
    var listenerId: String? = null,
    var dateTime: String? = null,
    var distance: Float? = null
)