package com.example.individualprojectcovidapp

import android.content.Context
import android.util.Log
import java.util.*

class PreferencesUtilities(
    private val activity: MainActivity,
    private val applicationContext: Context
) {

    /**
     * Checks if the currently running application instance has a UUID stored in the
     * SharedPreferences file, and if not, creates a new one and assigns it to the global variable
     * in MainActivity.
     */
    fun createUUID() {
        val sharedPref = applicationContext.getSharedPreferences(
            applicationContext.getString(R.string.preference_file_key), Context.MODE_PRIVATE
        )
        if (!sharedPref.contains(applicationContext.getString(R.string.app_instance_uuid))) {
            val newUUID = UUID.randomUUID().toString()
            with(sharedPref.edit()) {
                putString(applicationContext.getString(R.string.app_instance_uuid), newUUID)
                apply()
            }
        }
        activity.UUID = getInstanceUUID()
    }

    /**
     * Returns the UUID of the currently running instance of the application from the
     * SharedPreferences file.
     *
     * @return  The instance UUID
     */
    private fun getInstanceUUID(): String {
        val sharedPref = applicationContext.getSharedPreferences(
            applicationContext.getString(R.string.preference_file_key), Context.MODE_PRIVATE
        )
        return sharedPref.getString(applicationContext.getString(R.string.app_instance_uuid), "")
            .toString()
    }


}