package com.example.individualprojectcovidapp

import android.bluetooth.le.AdvertiseCallback
import android.bluetooth.le.AdvertiseSettings
import android.content.Context
import android.os.Handler
import android.os.Looper
import android.os.RemoteException
import android.util.Log
import org.altbeacon.beacon.*
import org.altbeacon.beacon.service.RunningAverageRssiFilter


class BeaconUtilities(
    private val activity: MainActivity,
    private val applicationContext: Context,
) {
    private var TAG: String = "BeaconUtilities"
    private var beaconManager = BeaconManager.getInstanceForApplication(activity)
    private var firebaseUtilities = FirebaseUtilities()

    /**
     * Binds the BeaconManager object to the MainActivity.
     *
     * @throws RemoteException
     */
    fun bindBeaconManager() {
        try {
            beaconManager.bind(activity)
        } catch (e: RemoteException) {
            Log.e(TAG, "Cannot talk to service")
        }
    }

    /**
     * Unbinds the BeaconManager object from the MainActivity.
     */
    fun unbindBeaconManager() {
        beaconManager.unbind(activity)
    }

    /**
     * Begins broadcasting Bluetooth beacons with the AltBeacon format
     *
     * @throws RemoteException
     */
    fun startBroadcast() {
        try {
            val beacon = Beacon.Builder()
                .setId1(activity.UUID)
                .setId2("1")
                .setId3("2")
                .setManufacturer(0x0118) // Radius Networks.
                .setTxPower(-59)
                .build()
            val beaconParser = BeaconParser()
                .setBeaconLayout("m:2-3=beac,i:4-19,i:20-21,i:22-23,p:24-24")
            val beaconTransmitter = BeaconTransmitter(applicationContext, beaconParser)
            beaconTransmitter.startAdvertising(beacon, object : AdvertiseCallback() {
                override fun onStartFailure(errorCode: Int) {
                    Log.e(TAG, "Advertisement start failed with code: $errorCode")
                }

                override fun onStartSuccess(settingsInEffect: AdvertiseSettings) {
                    Log.i(TAG, "Advertisement start succeeded.")
                }
            })
        } catch (e: RemoteException) {
            Log.e(TAG, "Cannot start broadcast")
        }
    }

    /**
     * Begins listening for Bluetooth beacons from nearby devices
     *
     * @throws RemoteException
     */
    fun startListening() {
        beaconManager.removeAllMonitorNotifiers()
        beaconManager.addMonitorNotifier(object : MonitorNotifier {
            override fun didEnterRegion(region: Region?) {
                Log.i(TAG, "I just saw a beacon for the first time!")
            }

            override fun didExitRegion(region: Region?) {
                Log.i(TAG, "I no longer see a beacon")
            }

            override fun didDetermineStateForRegion(state: Int, region: Region?) {
                Log.i(TAG, "I have just switched from seeing/not seeing beacons: $state")
            }
        })
        try {
            beaconManager.startMonitoringBeaconsInRegion(
                Region(
                    "myMonitoringUniqueId",
                    null,
                    null,
                    null
                )
            )
        } catch (e: RemoteException) {
        }
    }

    /**
     * Updates the time periods for scanning for nearby beacons and waiting between scans.
     *
     * @param scanPeriod            The period of time the app should scan for nearby beacons
     * @param betweenScanPeriod     The period of time to wait between scans
     * @throws RemoteException
     */
    fun updateForegroundScanTimes(scanPeriod: Int, betweenScanPeriod: Int) {
        try {
            beaconManager.foregroundScanPeriod = scanPeriod.toLong() // 100 mS
            beaconManager.foregroundBetweenScanPeriod = betweenScanPeriod.toLong() // 0ms
            beaconManager.updateScanPeriods()
        } catch (e: RemoteException) {
            Log.e(TAG, "Cannot talk to service")
        }
    }

    /**
     * Updates the time period that the application will average the results over in order to
     * increase the accuracy for the calculated distance estimates.
     *
     * @param averagingTime            The period of time the app averages results over
     */
    fun updateAveragingTime(averagingTime: Int) {
        BeaconManager.setRssiFilterImplClass(RunningAverageRssiFilter::class.java)
        RunningAverageRssiFilter.setSampleExpirationMilliseconds(averagingTime.toLong())
    }

    /**
     * Begins ranging Bluetooth beacons from nearby devices
     *
     * @throws RemoteException
     */
    fun startRanging() {
        Log.i(TAG, "RANGING")
        beaconManager.removeAllRangeNotifiers()
        beaconManager.addRangeNotifier { beacons, region ->
            checkBeaconsWithinTwoMetres(beacons)
            if (beacons.isNotEmpty()) {
                Log.i(
                    TAG,
                    "The first beacon I see is about " + beacons.iterator()
                        .next().distance + " meters away."
                )
            }
        }
        try {
            beaconManager.startRangingBeaconsInRegion(
                Region(
                    "myRangingUniqueId",
                    null,
                    null,
                    null
                )
            )
        } catch (e: RemoteException) {
        }
    }

    /**
     * Check if beacons are detected within 2 metres and makes method call to update the user
     * interface accordingly
     *
     * @param beacons list of all beacons detected by the app
     */
    private fun checkBeaconsWithinTwoMetres(beacons: Collection<Beacon>) {
        val beaconsWithinTwoMetres = getBeaconsWithinTwoMetres(beacons)
        val handler = Handler(Looper.getMainLooper())
        handler.post {
            activity.updateViewText(beaconsWithinTwoMetres.isNotEmpty())
        }
    }

    /**
     * Returns a mutable list of all of the beacons detected within 2 metres
     *
     * @param beacons   List of all beacons detected by the app
     * @return          List of beacons within 2 metres
     */
    private fun getBeaconsWithinTwoMetres(beacons: Collection<Beacon>): MutableList<Beacon> {
        val beaconsWithinTwoMetres = mutableListOf<Beacon>()
        beacons.forEach { beacon ->
            if (beacon.distance < 2) {
                firebaseUtilities.writeNewInteraction(
                    beacon.id1.toString(),
                    activity.UUID,
                    firebaseUtilities.getCurrentDate(),
                    beacon.distance.toFloat()
                )
                beaconsWithinTwoMetres.add(beacon)
            }
        }
        return beaconsWithinTwoMetres
    }
}