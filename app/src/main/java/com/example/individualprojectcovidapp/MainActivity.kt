package com.example.individualprojectcovidapp

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import org.altbeacon.beacon.*


class MainActivity : AppCompatActivity(), BeaconConsumer {
    var UUID: String = ""

    private var beaconUtilities: BeaconUtilities? = null
    private var preferencesUtilities: PreferencesUtilities? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar))

        preferencesUtilities = PreferencesUtilities(this, applicationContext)
        preferencesUtilities?.createUUID()

        beaconUtilities = BeaconUtilities(this, applicationContext)
        beaconUtilities!!.bindBeaconManager()
        beaconUtilities?.updateForegroundScanTimes(1000, 1000)
        beaconUtilities?.updateAveragingTime(10000)
        beaconUtilities?.startBroadcast()
    }

    override fun onBeaconServiceConnect() {
        beaconUtilities?.startListening()
        beaconUtilities?.startRanging()
    }

    override fun onDestroy() {
        super.onDestroy()
        beaconUtilities?.unbindBeaconManager()
    }

    /**
     * Updates the TextView in FirstFragment to notify the user of nearby interactions based on
     * whether devices are detected within 2 metres
     *
     * @param beaconsWithinTwoMetres    a boolean value specifying if beacons have been detected
     *                                  within 2 metres off the device
     */
    fun updateViewText(beaconsWithinTwoMetres: Boolean) {
        val textView = findViewById<View>(R.id.nearby_devices_text) as TextView
        if (beaconsWithinTwoMetres) {
            textView.text = getString(R.string.nearby_devices_detected)
            textView.setTextColor(
                ContextCompat.getColor(
                    applicationContext,
                    R.color.leeds_red_dark
                )
            )
        } else {
            textView.text = getString(R.string.no_nearby_devices)
            textView.setTextColor(
                ContextCompat.getColor(
                    applicationContext,
                    R.color.leeds_green_dark
                )
            )
        }
    }
}
