# Individual Project Covid App
A Mobile Application Detecting Proximity Between Devices to Assist Social Distancing in the COVID-19 Pandemic.
Technologies used: 
- Kotlin
- Bluetooth Low Energy

# How to Download and Run the Application
## Requirements:
- Android Studio
## Steps:
1. Pull master branch
2. Open the Project in Android Studio using the "Open an Existing Project" button
![The startup page in Android Studio](/images/loading-AS.png)*The startup page in Android Studio*
## Running the Application on Physical Devices:
1. Plug in device(s)
2. Select device from the devices dropdown menu
3. Click run
![The devices menu and run button in Android Studio](/images/devices-AS.png)*The devices menu and run button in Android Studio*
